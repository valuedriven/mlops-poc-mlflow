import plotly.express as px


def metrics_plot(metrics_df):
    fig = px.bar(metrics_df,
                 x='method',
                 y=['accuracy', 'precision', 'recall', 'f1'],
                 barmode='group', text_auto='.2f', title='Metrics by method')
    fig.show()
