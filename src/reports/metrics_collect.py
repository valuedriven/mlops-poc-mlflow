import pandas as pd
from sklearn.metrics import accuracy_score, precision_score
from sklearn.metrics import f1_score, recall_score, confusion_matrix


def metrics_collect(method, y_test, y_pred):
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1_metric = f1_score(y_test, y_pred)
    confusion_matrix_data = confusion_matrix(y_test, y_pred, normalize='true')
    results = [{'confusion_matrix': confusion_matrix_data}]
    confusion_matrix_json = pd.Series(results).to_json(orient='records')
    metrics_data = {'method': method,
                    'accuracy': accuracy,
                    'precision': precision,
                    'recall': recall,
                    'f1': f1_metric,
                    'confusion_matrix': confusion_matrix_json}
    return metrics_data
