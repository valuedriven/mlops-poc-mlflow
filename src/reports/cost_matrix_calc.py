import numpy as np


def cost_matrix_calc(metrics_df, cost_matrix_values):
    cost_matrix = cost_matrix_values
    costs = []
    for ind in metrics_df.index:
        cost = np.sum(np.multiply(
            cost_matrix, metrics_df['confusion_matrix'][ind]))
        costs.append(cost)
    metrics_df['cost'] = costs

    method_min_cost = metrics_df['method'][metrics_df['cost'].idxmin()]
    print("Method with the minimun cost: ", method_min_cost)
