import json
from os import path


def metrics_log(metrics_file, metrics):
    list_obj = []

    if path.isfile(metrics_file) is True:
        with open(metrics_file, encoding="utf-8") as working_file:
            list_obj = json.load(working_file)

    list_obj.append(metrics)

    with open(metrics_file, 'w', encoding="utf-8") as metrics_file_alias:
        json.dump(
            list_obj,
            metrics_file_alias,
            indent=4,
            separators=(',', ': '))
