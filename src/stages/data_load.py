from sklearn.datasets import fetch_openml
from src.utils.logs import get_logger
from src.utils.load_config import load_config
from src.utils.get_args import get_args


def data_load(config_file='params.yaml'):
    config = load_config(config_file)
    remote_dataset = config["data"]["remote_dataset"]
    local_dataset = config["data"]["local_dataset"]
    log_level = config['base']['log_level']
    logger = get_logger('DATA_LOAD', log_level=log_level)

    logger.info('Get remote dataset')
    remote_data = fetch_openml(name=remote_dataset, version='1',
                               parser='auto', as_frame=True)
    dataset = remote_data.frame

    logger.info('Save local dataset')
    dataset.to_csv(local_dataset, index=False)
    return dataset


if __name__ == '__main__':
    args = get_args()
    data_load(config_file=args.config)
