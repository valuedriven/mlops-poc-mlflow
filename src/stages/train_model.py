import json
import pandas as pd
import numpy as np
import joblib
from src.utils.logs import get_logger
from src.utils.load_config import load_config
from src.utils.get_args import get_args
from src.utils.train import train


def train_model(config_file='params.yaml'):

    config = load_config(config_file)
    x_train_file = config["data"]["x_trainset_path"]
    y_train_file = config["data"]["y_trainset_path"]
    models_path = config['train']['model_path']
    params_path = config['train']['params_path']
    estimator_name = config['train']['estimator_name']
    log_level = config['base']['log_level']

    logger = get_logger('TRAIN_MODEL', log_level=log_level)

    logger.info('Estimator: %s', estimator_name)

    logger.info('Load train dataset')
    x_train = pd.read_csv(x_train_file).to_numpy()
    y_train = np.ravel(pd.read_csv(y_train_file))

    logger.info('Train model')
    model = train(
        x_train=x_train,
        y_train=y_train,
        estimator_name=estimator_name,
        param_grid=config['train']['estimators'][estimator_name]['param_grid'],
        **config['train']['grid_search_cv_params']
    )

    logger.info('Save model')
    joblib.dump(model, models_path)
    model_params = str(model.get_params(deep=True))
    logger.info('Model params: %s', model_params)

    logger.info('Save params')
    with open(params_path, "w", encoding='utf-8') as outfile:
        json.dump(model_params, outfile)


if __name__ == '__main__':
    args = get_args()
    train_model(config_file=args.config)
