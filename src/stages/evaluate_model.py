import json
from pathlib import Path
import joblib
import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix, f1_score
from src.reports.visualize import plot_confusion_matrix
from src.utils.logs import get_logger
from src.utils.load_config import load_config
from src.utils.get_args import get_args


def evaluate_model(config_file='params.yaml'):

    config = load_config(config_file)
    logger = get_logger('EVALUATE', log_level=config['base']['log_level'])

    logger.info('Load model')
    model = joblib.load(config['train']['model_path'])

    logger.info('Load test dataset')
    x_test = pd.read_csv(config["data"]["x_testset_path"]).to_numpy()
    y_test = np.ravel(pd.read_csv(config["data"]["y_testset_path"]))

    logger.info('Evaluate (predict)')
    prediction = model.predict(x_test)

    logger.info('Evaluate (calculate f1 score)')
    # f1_score_object = f1_score(
    #     y_true=y_test, y_pred=prediction, average='macro')
    f1_score_object = f1_score(y_true=y_test, y_pred=prediction)

    logger.info('Evaluate (build report)')
    confusion_matrix_object = confusion_matrix(y_test, prediction)
    report = {
        'f1': f1_score_object,
        'cm': confusion_matrix_object,
        'actual': y_test,
        'predicted': prediction
    }

    logger.info('Save metrics')
    reports_folder = Path(config['evaluate']['reports_dir'])
    metrics_path = reports_folder / config['evaluate']['metrics_file']

    with open(metrics_path, "w", encoding='utf-8') as outfile:
        json.dump({'f1_score': report['f1']}, outfile)

    logger.info('F1 metrics file saved to: %s', metrics_path)

    logger.info('Save confusion matrix')
    target_names = load_iris(as_frame=True).target_names.tolist()
    plt = plot_confusion_matrix(confusion_matrix=report['cm'],
                                target_names=target_names,
                                normalize=False)
    confusion_matrix_png_path = reports_folder / \
        config['evaluate']['confusion_matrix_image']
    plt.savefig(confusion_matrix_png_path)


if __name__ == '__main__':
    args = get_args()
    evaluate_model(config_file=args.config)
