from dataclasses import dataclass
from sklearn.model_selection import GridSearchCV


@dataclass
class TrainModelParams:
    model: str
    model_params: str
    grid_search_cv_params: str
    x_train: str
    y_train: str
    x_test: str


def train_model(params: TrainModelParams):
    classifier = GridSearchCV(
        params.model, params.model_params, **params.grid_search_cv_params)
    classifier.fit(params.x_train, params.y_train)
    params.model.set_params(**classifier.best_params_)
    trained_model = params.model.fit(
        params.x_train, params.y_train).predict(params.x_test)
    return trained_model
