from urllib.parse import urlparse

import pandas as pd
import numpy as np

import mlflow
from mlflow.models import infer_signature
import mlflow.sklearn

import logging

from utils.load_config import load_config
from utils.get_args import get_args
from utils.eval_metrics import eval_metrics
from train import train

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('credits_train')


def main(args):
    with mlflow.start_run():
        config = load_config(args.config_file)
        x_train_file = args.x_trainset_file
        y_train_file = args.y_trainset_file
        x_test_file = args.x_testset_file
        y_test_file = args.y_testset_file
        estimator_name = config['train']['estimator_name']

        logger.info('Estimator: %s', estimator_name)

        logger.info('Load train dataset')
        x_train = pd.read_csv(x_train_file).to_numpy()
        y_train = np.ravel(pd.read_csv(y_train_file))
        x_test = pd.read_csv(x_test_file).to_numpy()
        y_test = np.ravel(pd.read_csv(y_test_file))

        logger.info('Train model')
        model = train(
            x_train=x_train,
            y_train=y_train,
            estimator_name=estimator_name,
            param_grid=config['train']['estimators'][estimator_name]['param_grid'],
            **config['train']['grid_search_cv_params']
        )

        predicted_qualities = model.predict(x_test)

        (rmse, mae, r2, accuracy, precision, recall,
         f1) = eval_metrics(y_test, predicted_qualities)
        collected_metrics = {'rmse': rmse, 'mae': mae, 'r2': r2,
                             'accuracy': accuracy, 'precision': precision, 'recall': recall, 'f1': f1}
        logger.info(f"Collected metrics: {collected_metrics}")
        model_params = model.get_params(deep=True)
        mlflow.log_params(model_params)
        mlflow.log_metrics(collected_metrics)

        predictions = model.predict(x_train)
        signature = infer_signature(x_train, predictions)

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        logger.info(f"tracking_url_type_store: {tracking_url_type_store}")

        if tracking_url_type_store != "file":
            mlflow.sklearn.log_model(
                model, "model", registered_model_name="CreditsModel", signature=signature
            )
        else:
            mlflow.sklearn.log_model(model, "model", signature=signature)


if __name__ == '__main__':
    args = get_args()
    main(args)
