import argparse


def get_args():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config_file', required=True)
    args_parser.add_argument('--x_trainset_file', dest='x_trainset_file')
    args_parser.add_argument('--y_trainset_file', dest='y_trainset_file')
    args_parser.add_argument('--x_testset_file', dest='x_testset_file')
    args_parser.add_argument('--y_testset_file', dest='y_testset_file')
    args = args_parser.parse_args()
    return args
