import boto3

service_name = 's3'
endpoint_url = 'https://storagegw.estaleiro.serpro.gov.br/'
bucket = 'dvcbucket'

session = boto3.session.Session()

s3_client = session.client(
    service_name=service_name,
    endpoint_url=endpoint_url
)

# s3_client.put_bucket_versioning(
#     Bucket=bucket,
#     VersioningConfiguration={
#         'Status': 'Enabled'
#     }
# )

# print(s3_client.get_bucket_versioning(Bucket=bucket))

# attributes = ['ETag', 'Checksum', 'ObjectParts', 'StorageClass', 'ObjectSize']
attributes = ['ETag']

response = s3_client.list_objects(
    Bucket=bucket
)

print('list objects')
print(response)

response = s3_client.get_object_attributes(
    Bucket=bucket,
    Key='models/model.joblib',
    ObjectAttributes=attributes
)

print('list object details')
print(response)
